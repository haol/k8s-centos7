本次安装的是**k8s v1.16.0**，为啥是这个，请搜索《CentOS搭建K8S，一次性成功》的文章。

配置建议2cpu 2g内存。1g内存实验不一定跑得起来。

首先安装docker：

```bash
# 安装docker所需的工具  
yum install -y yum-utils device-mapper-persistent-data lvm2  
# 配置阿里云的docker源  
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo  
# 指定安装这个版本的docker-ce  
yum install -y docker-ce-18.09.9-3.el7 
# 启动docker  
systemctl enable docker && systemctl start docker 
```

设置系统环境：

> 1.  # 关闭防火墙 
> 2.  systemctl disable firewalld 
> 3.  systemctl stop firewalld 
> 4.  # 关闭selinux 
> 5.  # 临时禁用selinux 
> 6.  setenforce 0 
> 7.  # 永久关闭 修改/etc/sysconfig/selinux文件设置 
> 8.  sed -i 's/SELINUX=permissive/SELINUX=disabled/' /etc/sysconfig/selinux 
> 9.  sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config 
> 10.  # 禁用交换分区 
> 11.  swapoff -a 
> 12.  # 永久禁用，打开/etc/fstab注释掉swap那一行。 
> 13.  sed -i 's/.\*swap.\*/#&/' /etc/fstab 
> 14.  # 修改内核参数 
> 15.  cat >>/etc/sysctl.d/k8s.conf<<EOF
> 16.  net.bridge.bridge-nf-call-ip6tables = 1
> 17.  net.bridge.bridge-nf-call-iptables = 1
> 18.  EOF
> 19.  sysctl --system 
> 20.  #设置hostname:
> 21.  \# 主节点  
>     hostnamectl set-hostname k8s-master  
>     \# 从节点  
>     hostnamectl set-hostname k8s-node1
> 22.  #修改hosts文件
> 23.  cat >>/etc/hosts<<EOF  
>     192.168.0.122 k8s-master  
>     192.168.0.123 k8s-node1  
>     EOF

安装kubeadm、kubelet、kubectl

> 1.  # 执行配置k8s阿里云源 
> 2.  vi /etc/yum.repos.d/kubernetes.repo 
> 3.  \[kubernetes\] 
> 4.  name=Kubernetes
> 5.  baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/ 
> 6.  enabled=1
> 7.  gpgcheck=1
> 8.  repo_gpgcheck=1
> 9.  gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg 
> 10.  # 安装kubeadm、kubectl、kubelet 
> 11.  yum install -y kubectl-1.16.0-0 kubeadm-1.16.0-0 kubelet-1.16.0-0 
> 12.  # 启动kubelet服务 
> 13.  systemctl enable kubelet && systemctl start kubelet 

初始化k8s，这里本来是需要访问google的，由于众所周知的原因，我们用ali的k8s源代替。

> 1.  # 执行配置k8s阿里云源 
> 2.  cat>>/etc/yum.repos.d/kubernetes.repo  <<EOF
> 3.  \[kubernetes\] 
> 4.  name=Kubernetes
> 5.  baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/ 
> 6.  enabled=1
> 7.  gpgcheck=1
> 8.  repo_gpgcheck=1
> 9.  gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg 
> 10.  EOF 
> 11.  # 安装kubeadm、kubectl、kubelet 
> 12.  yum install -y kubectl-1.16.0-0 kubeadm-1.16.0-0 kubelet-1.16.0-0 
> 13.  # 启动kubelet服务 
> 14.  systemctl enable kubelet && systemctl start kubelet 

初始化k8s，需要注意192.168.99.104需要改为自己的k8s-master的ip地址。

> 1.  # 下载管理节点中用到的6个docker镜像，你可以使用docker images查看到 
> 2.  # 这里需要大概两分钟等待，会卡在\[preflight\] You can also perform this action in beforehand using ''kubeadm config images pull 
> 3.  kubeadm init --image-repository registry.aliyuncs.com/google_containers --kubernetes-version v1.16.0 --apiserver-advertise-address 192.168.99.104 --pod-network-cidr=10.244.0.0/16 --token-ttl 0 

结束后按提示运行：

> 1.  # 上面安装完成后，k8s会提示你输入如下命令，执行 
> 2.  mkdir -p $HOME/.kube 
> 3.  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config 
> 4.  sudo chown $(id -u):$(id -g) $HOME/.kube/config  

 并保存子节点加入master的命令，如果没保存，可以在master上运行以下命令查看：

> 1.  kubeadm token create --print-join-command 

> 检查：运行
> 
> kubectl get nodes 
> 
> 提示k8s-master not ready是正常的。

接下来安装子节点：

docker，系统环境，阿里k8s源配置参照上文。

子节点不再需要安装kubectl，仅安装安装kubeadm、kubelet 

> 1.  # 安装kubeadm、kubectl、kubelet 
> 2.  yum install -y  kubeadm-1.16.0-0 kubelet-1.16.0-0 
> 3.  # 启动kubelet服务 
> 4.  systemctl enable kubelet && systemctl start kubelet 

加入集群 这里加入集群的命令每个人都不一样，可以登录master节点，使用kubeadm token create --print-join-command 来获取。获取后执行

加入成功后，可以在master节点上使用kubectl get nodes命令查看到加入的节点。

**安装flannel（master机器）**

以上步骤安装完后，机器搭建起来了，但状态还是NotReady状态，如下图，master机器需要安装flanneld。     

下载官方fannel配置文件 使用wget命令，地址为：(https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml)，这个地址国内访问不了，而且其中配置了一个国内无法访问的地址(quay.io)，因此使用国内镜像版本：

其中的quay地址可以改为国内镜像，之前的qiniu镜像服务器应该无法访问了，我使用了_quay_.mirrors.ustc.edu.cn地址    

> wget [https://gitee.com/haol/k8s-centos7/raw/master/kube-flannel.yml](https://gitee.com/haol/k8s-centos7/raw/master/kube-flannel.yml)
> 

2.  安装fannel

> kubectl apply -f kube-flannel.yml 

至此，k8s集群搭建完成，节点已为Ready状态。

之后就到了dashboard的安装了，首先要看一下我们1.16.0能用的最终版本：https://github.com/kubernetes/dashboard/releases，我已经看过了是2.0.5，大家直接下载这个版本就好了，原始链接是：

> kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.5/aio/deploy/recommended.yaml

我已经放到了我的git里了，大家用这个就可以，一个字都没改：

> kubectl apply -f https://gitee.com/haol/k8s-centos7/raw/master/kubernetes-dashboard.yml

检查是否运行正常：

> kubectl get pod -n kubernetes-dashboard 

访问dashboard需要发布端口，最直接的是kubectl proxy，发布到局域网访问需要ssl证书，这个要单独给kubectl proxy搞证书似乎很麻烦，建议还是本机访问使用
看到状态时running就可以了，而后启动api访问代理
> kubectl proxy --address='0.0.0.0' --port=8888 --access-hosts='^*$'

之后可以通过如下url访问：
http://127.0.0.1:8888/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

如果要通过其他ip访问，最简单的方法是通过NodePort发布端口访问，需要修改kubernetes-dashboard.yml
将
```
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  ports:
    - port: 443
      targetPort: 8443
  selector:
    k8s-app: kubernetes-dashboard
```

修改为：

```
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  type: NodePort
  ports:
    - port: 443
      targetPort: 8443
      nodePort:30001
  selector:
    k8s-app: kubernetes-dashboard
```
端口需要大于30000小于32767


创建登录用户，官方参考文档：https://github.com/kubernetes/dashboard/wiki/Creating-sample-user

创建dashboard-adminuser.yaml：


```
    apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: admin-user
      namespace: kube-system
    ---
    apiVersion: rbac.authorization.k8s.io/v1
    kind: ClusterRoleBinding
    metadata:
      name: admin-user
    roleRef:
      apiGroup: rbac.authorization.k8s.io
      kind: ClusterRole
      name: cluster-admin
    subjects:
      - kind: ServiceAccount
        name: admin-user
        namespace: kube-system
```


`kubectl apply -f dashboard-adminuser.yaml`

说明：上面创建了一个叫admin-user的服务账号，并放在kube-system命名空间下，并将cluster-admin角色绑定到admin-user账户，这样admin-user账户就有了管理员的权限。默认情况下，kubeadm创建集群时已经创建了cluster-admin角色，我们直接绑定即可。


获取token：

`kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')`


将最后的token复制到网页界面就可以访问啦！！！